---
title: Michal Gebauer
navbar: false
---

# Michal Gebauer

[:envelope: mishak@mishak.net](mailto:mishak@mishak.net)  
[:necktie: linkedin.com/in/michalgebauer](https://linkedin.com/in/michalgebauer/)  
[:bird: twitter.com/mishak87](https://twitter.com/mishak87)  
[👨‍💻 gitlab.com/mishak](https://gitlab.com/mishak)  
[👨‍💻 github.com/mishak87](https://github.com/mishak87)
